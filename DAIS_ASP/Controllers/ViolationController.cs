﻿using DAIS_ORM.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DAIS_ASP.Controllers
{
    public class ViolationController : Controller
    {
        //
        // GET: /Violation/
        public ActionResult Index()
        {
            ViewBag.Violations = ViolationTable.Select();
            ViewBag.Title = "Violations";

            return View();
        }

        public ActionResult Detail(Int32 Id)
        {
            Violation violation = ViolationTable.Select(Id);

            if (violation == null)
            {
                throw new HttpException(404, "Not Found");
            }

            ViewBag.Tickets = TicketTable.SelectV(Id);
            ViewBag.Violation = violation;
            ViewBag.Title = "Violation detail";

            return View();
        }

        [HttpGet]
        public ActionResult Violator()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Violator(String violator_birth_number)
        {
            Violator violator = ViolatorTable.Select(violator_birth_number);

            ViewBag.Violator = violator;
            ViewBag.Violations = ViolationTable.SelectVio(violator.Id);

            return View("Violator");
        }

        public ActionResult Pay(Int32 Id)
        {
            try
            {
                Ticket t = TicketTable.Select(Id);
                Procedures.PayTicket(t.Id, "cash");
            }
            catch (Exception)
            {
                throw new HttpException(500, "Error");
            }

            return Redirect("/Violation/Detail/" + Id);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Title = "New Violation";

            return View();
        }

        [HttpPost]
        public ActionResult Create(Int16? b)
        {
            try
            {
                Int32 number = Int32.Parse(Request.Form["officer_number"]);
                String birth_number = Request.Form["violator_birth_number"];
                String first_name = Request.Form["violator_first_name"];
                String last_name = Request.Form["violator_last_name"];
                String phone = Request.Form["violator_phone"];
                String state = Request.Form["violator_state"];
                String street = Request.Form["violator_street"];
                String city = Request.Form["violator_city"];
                String zip = Request.Form["violator_zip"];
                String licence = Request.Form["vehicle_licence"];
                String manufacturer = Request.Form["vehicle_manufacturer"];
                String model = Request.Form["vehicle_model"];
                Int32 year = Int32.Parse(Request.Form["vehicle_year"]);
                String type = Request.Form["violation_type"];
                Int32 amount = Int32.Parse(Request.Form["violation_amount"]);
                DateTime date = DateTime.Parse(Request.Form["violation_date"]);

                Procedures.NewTicket(number, birth_number, first_name, last_name, phone, state, street, city, zip, licence, manufacturer, model, year, type, amount, date);

                return View("CreateSuccess");
            }
            catch (Exception e)
            {
                return View("CreateFailure");
            }
        }
	}
}