﻿using DAIS_ORM.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DAIS_ASP.Controllers
{
    public class OfficerController : Controller
    {
        //
        // GET: /Officer/
        public ActionResult Index()
        {
            ViewBag.Officers = OfficerTable.Select();
            ViewBag.Title = "Officers";

            return View();
        }

        public ActionResult Detail(Int32 Number)
        {
            Officer officer = OfficerTable.Select(Number);
            
            ViewBag.Officer = officer;
            ViewBag.Violations = ViolationTable.SelectOff(officer.Number);
            ViewBag.Title = "Officer detail";

            return View();
        }
	}
}