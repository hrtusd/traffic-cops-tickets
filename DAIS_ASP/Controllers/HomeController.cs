﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAIS_ORM.Database;

namespace DAIS_ASP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Index";

            return View();
        }

        public ActionResult Test()
        {
            Collection<Tuple<Violator, Collection<Violation>>> kokot = Procedures.ViolationsViolator();


            ViewBag.List = kokot;

            return View();
        }

        public ActionResult Test2()
        {
            System.Collections.Generic.IEnumerable<Vehicle> vehicles = VehicleTable.Select();

            return View(vehicles);
        
        }
        public ActionResult Test3()
        {

            return View();
        }

        public ActionResult Test4()
        {
            IEnumerable<Violation> v = ViolationTable.Select();

            return View(v);
        }
    }
}