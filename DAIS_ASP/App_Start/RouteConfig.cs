﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DAIS_ASP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Officer",
                url: "Officer/{action}/{Number}",
                defaults: new { controller = "Officer", action = "Index" }
            );

            routes.MapRoute(
                name: "Violation",
                url: "Violation/{action}/{Id}",
                defaults: new { controller = "Violation", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}
