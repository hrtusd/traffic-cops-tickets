﻿using DAIS_ORM.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DAIS_ASP.Areas.Admin.Controllers
{
    public class StationController : Controller
    {
        //
        // GET: /Admin/Station/
        public ActionResult Index()
        {
            IEnumerable<Station> stations = StationTable.Select();

            return View(stations);
        }

        //
        // GET: /Station/Details/5
        public ActionResult Details(int id)
        {
            Station station = StationTable.Select(id);

            if (station == null)
            {
                throw new HttpException(404, "Not found");
            }

            return View(station);
        }

        //
        // GET: /Station/Create
        public ActionResult Create()
        {
            IEnumerable<Address> addr = AddressTable.Select();

            ViewBag.Addresses = addr;

            return View();
        }

        //
        // POST: /Station/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Station station = new Station();
                station.Name = collection["Name"];
                Address address = AddressTable.Select(Int32.Parse(collection["Address"]));
                station.Address = address;
                StationTable.Insert(station);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Station/Edit/5
        public ActionResult Edit(int id)
        {
            Station station = StationTable.Select(id);

            if (station == null)
            {
                throw new HttpException(404, "Not found");
            }

            return View(station);
        }

        //
        // POST: /Station/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Station station = StationTable.Select(id);
                station.Name = collection["Name"];
                StationTable.Update(station);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Station/Delete/5
        public ActionResult Delete(int id)
        {
            Station station = StationTable.Select(id);

            if (station == null)
            {
                throw new HttpException(404, "Not found");
            }

            return View(station);
        }

        //
        // POST: /Station/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                StationTable.Delete(StationTable.Select(id));

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
	}
}